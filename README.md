## 功能介绍 
公园预约小程序是一种基于微信小程序平台开发的应用程序，主要用于公园预约服务。用户可以通过该小程序在线预约公园的场地、设施、活动等服务，避免了传统的人工预约方式的繁琐和耗时。同时，公园管理部门也可以通过该小程序实现对公园资源的有效管理和利用，提高公园服务的效率和质量

    
公园预约小程序前后端完整代码，包括公园动态，公园简介，入园预约，停车预约等功能，采用腾讯提供的小程序云开发解决方案，无须服务器和域名。由于入园市民、游客较多，为更好保障疫情防控和游客游园安全，公园可以采取网上小程序预约方式控制入园游客在可容峰值内，从而方面管理者和广大市民合理安排时间，错峰出行。

- 预约管理：开始/截止时间/人数均可灵活设置，可以自定义客户预约填写的数据项
- 预约凭证：支持线下到场后校验签到/核销/二维码自助签到等多种方式
- 详尽的预约数据：支持预约名单数据导出Excel， 打印

  ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信:
 
![输入图片说明](demo/author-base.png)


## 演示 

   ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
 

## 安装

- 安装手册见源码包里的word文档




## 截图
![输入图片说明](demo/1%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/2%E5%85%AC%E5%9B%AD%E5%8A%A8%E6%80%81.png)
![输入图片说明](demo/3%E5%85%AC%E5%9B%AD%E7%AE%80%E4%BB%8B.png)
 ![输入图片说明](demo/4%E9%A2%84%E7%BA%A6%E6%97%A5%E5%8E%86.png)
![输入图片说明](demo/5%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/6%E5%85%A5%E5%9B%AD%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/7%E5%81%9C%E8%BD%A6%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/8%E9%A2%84%E7%BA%A6%E8%AF%A6%E6%83%85.png)
![输入图片说明](demo/9%E9%A2%84%E7%BA%A6%E6%8A%A5%E5%90%8D.png)
![输入图片说明](demo/10%E9%A2%84%E7%BA%A6%E6%88%90%E5%8A%9F.png)
## 后台管理系统截图
 
![输入图片说明](demo/11%E5%90%8E%E5%8F%B0%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/12%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/13%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E8%8F%9C%E5%8D%95.png)
![输入图片说明](demo/14%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%AF%BC%E5%87%BA.png)
![输入图片说明](demo/15%E5%90%8E%E5%8F%B0-%E6%A0%B8%E9%94%80%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/16%E5%90%8E%E5%8F%B0%E5%90%8D%E5%8D%95%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/17%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%B7%BB%E5%8A%A0.png)
![输入图片说明](demo/18%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%97%B6%E6%AE%B5%E8%AE%BE%E7%BD%AE.png)

![输入图片说明](demo/19%E5%90%8E%E5%8F%B0-%E5%86%85%E5%AE%B9%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/20%E5%90%8E%E5%8F%B0-%E5%86%85%E5%AE%B9%E6%B7%BB%E5%8A%A0.png)